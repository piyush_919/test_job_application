class AppliedUsersController < ApplicationController
#before action
before_action :authenticate_user!
before_action :correct_user,   only: [:show]

#methods
	def create
		@job = Job.find(params[:job_id])
    @appliedjob = @job.applied_users.build(user_id: current_user.id)

      #apply for a job
      if @appliedjob.save
        # Mail to employer
        ApplyMailer.sample_email(@job, current_user).deliver_now
      	flash[:success] = "YEYYY!!!! Your Application send to Employer."
        redirect_to jobs_path
      else
    		flash[:danger] = "Sorry Some error occur.. try again or find another job"
    		redirect_to root_url
    	end
	end

	def destroy
    @job = Job.find(params[:id])
    @canceljob = AppliedUser.find_by(job_id: @job.id, user_id: current_user.id)

    #cancel job
    if @canceljob.destroy
      flash[:danger] = "Ohhh !!! you cancel you job application"
      redirect_to jobs_path

    else
      flash[:danger] = "Sorry Some error occur.. try again or find another job"
      redirect_to root_url
    end
  end

  def show
    if current_user.has_role? :admin
    @job = Job.find(params[:id])
    @applied_user = @job.applied_job_users.page(params[:page]).per(8)
    else
      redirect_to jobs_path
    end
  end

private

# Confirms the correct user.
    def correct_user
      @employer_id = Job.find(params[:id]).user_id
      redirect_to(root_url) unless current_user == User.find(@employer_id)
    end

end