class JobsController < ApplicationController
#before action
before_action :authenticate_user!

#methods
  def index
    @jobs = Job.all.page(params[:page]).per(8)
    if current_user.has_role? :admin
      @jobs = current_user.jobs.page(params[:page]).per(8)
    end
  end

  def view
    if current_user.has_role? :admin
      @jobs = current_user.jobs.page(params[:page]).per(8)
    else
      redirect_to jobs_path
    end
  end

  def new
	  @job = Job.new
  end

  def create
  	@job = current_user.jobs.build(job_params)
    if @job.save
      flash.now[:success] = "Job created!"
      redirect_to jobs_path
    else
      flash.now[:danger] = "Something went wrong"
      render 'new'
    end
  end

  def edit
    @job = Job.find(params[:id])
  end

  def update
    @job = Job.find(params[:id])
    @job.user = current_user
    if @job.update_attributes(job_params)
      flash.now[:success] = "Your have successfully updated your job"
      redirect_to root_path
    else
      render 'edit'
    end
  end

  def destroy
  	 @job = Job.find(params[:id])
  	 if @job.destroy
  	 	flash.now[:success] = "You job has been successfully deleted"
  	 	redirect_to jobs_path
  	 else
  	 	flash.now[:danger] = "Something my not be right"
  	 end
  end


#User- define variables
private

  def job_params
    params.require(:job).permit(:post, :experience, :salary)
  end

end