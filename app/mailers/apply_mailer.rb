class ApplyMailer < ApplicationMailer
	default from: "from@example.com"

  def sample_email(job, user)
  	@current_user = user
  	@job = job
    @id = job.user_id
    @user = User.find(@id)
    mail(to: @user.email, subject: 'Applied for a job')
  end
end
