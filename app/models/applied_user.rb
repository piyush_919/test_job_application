class AppliedUser < ApplicationRecord
#database relations
  belongs_to :job
  belongs_to :user
#validations
  validates :job_id, presence: true
  validates :user_id, presence: true

end
