class Job < ApplicationRecord
#database relation
  belongs_to :user
  has_many :applied_users, dependent: :destroy
  has_many :applied_job_users, through: :applied_users, source: "user"

#validations
  validates :post, presence: true
  validates :salary, presence: true, numericality: { only_integer: true }
  validates :experience, presence: true, numericality: { only_integer: true }

end
