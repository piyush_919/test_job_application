class User < ApplicationRecord
rolify

#database relation
  has_many :jobs, dependent: :destroy
  # has_many :applied_jobs, class_name: "AppliedUser",
  #                           foreign_key: 'user_id', dependent: :destroy
  # has_many :apply_jobs, through: :applied_jobs
  has_many :applied_users, dependent: :destroy
  has_many :applied_jobs, through: :applied_users, source: "job"

#devise
  # Include default devise modules. Others available are:
  #:lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :confirmable

  validates_confirmation_of :password

#User - define method
  def check_if_applied job
    applied_jobs.include?(job)
  end
end
