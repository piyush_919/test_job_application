Rails.application.routes.draw do


#devise
  devise_for :users, controllers: { registrations: 'users/registrations' }

#root page
  root 'static_pages#home'

#jobs
  resources :jobs
  get  '/new',  to: 'jobs#new'
  get  '/view', to: 'jobs#view'

# applied user
  resources :applied_users, only: [:destroy, :show] do
  	post ':job_id' => "applied_users#create", on: :collection, as: :new
  end
end
