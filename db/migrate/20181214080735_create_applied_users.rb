class CreateAppliedUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :applied_users do |t|
      t.integer :job_id
      t.integer :user_id

      t.timestamps
    end
    add_index :applied_users, :job_id
    add_index :applied_users, :user_id
    add_index :applied_users, [:job_id, :user_id], unique: true
  end
end
